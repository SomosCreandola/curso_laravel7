<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Support\Facades\DB;

Route::get('/', function () {
    return view('welcome');
});

/* Route::get('/', 'CartController@index')->name('cart.index'); */

/* Route::get('/test/{parametro?}', function ($parametro = null) {
    return 'mi numero es: '.$parametro;
}); */


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//example with querybuilder
route::get('/consulta-querybuilder', function(){
    //$fruits = DB::table('fruits')->orderBy('id', 'desc')->get();
    //$fruits = DB::table('fruits')->where('fruit', 'Aguacate')->first();
    //$fruits = DB::table('fruits')->count();
    $fruits = \DB::table('fruits')->max('price');
    echo "<pre>";
    var_dump($fruits);
    echo "</pre>";
});


//rutas de nuestro crud de test
Route::get('/test', 'TestController@index')->name('test.index');
Route::get('/create-test', 'TestController@create');
Route::post('/store-test', 'TestController@store')->name('test.store');







//crud de frutas con el ORM de laravel eloquent (query builder)
//php artisan route:list
Route::resource('/fruits', 'FruitController');

//rutas del modelo de productos
Route::get('/products-list', 'ProductController@index')->name('products.index');
Route::get('/products-create', 'ProductController@create')->name('products.create');
Route::get('/products-edit/{id}', 'ProductController@edit')->name('products.edit');
Route::post('/products-store', 'ProductController@store')->name('products.store');
Route::get('/products-path/{filename}', 'ProductController@getImgProduct')->name('products.path');
Route::put('/products-update/{id}', 'ProductController@update')->name('products.update');


//rutas del modelo de clientes
Route::resource('/clients', 'ClientController');

//rutas de herencia
Route::get('/herencia', function(){
    return view('test.herencia');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


//ejemplo herencia vistas de voyager
Route::get('/view-voyager', function(){
    $clientes = \DB::table('clients')->get();
    return view('voyager.test-view',[
        'clients' => $clientes
    ]);
});

//rutas de mi carrito de compras
Route::get('/carrito-index', 'CartController@index')->name('cart.index');
Route::get('/add-cart', 'CartController@addcart');
