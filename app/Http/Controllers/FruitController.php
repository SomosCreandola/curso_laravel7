<?php

namespace App\Http\Controllers;

use App\Fruit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FruitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //este metodo va a devolver todas las frutas que hay en la base de datos
        $fruits = Fruit::orderBy('id', 'desc')->paginate(5); 
        return view('fruits.index', [
            'fruits' => $fruits
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fruits.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fruit = new Fruit();
        $fruit->fruit = $request->input('fruit');
        $fruit->price = $request->input('price');
        $fruit->description = $request->input('description');

        if ($fruit->save()) {
            return redirect()->route('fruits.index')->with(['success' => 'Fruta '.$fruit->fruit.' creada exitosamente']);
        }
        //var_dump($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fruit  $fruit
     * @return \Illuminate\Http\Response
     */
    public function show(Fruit $fruit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fruit  $fruit
     * @return \Illuminate\Http\Response
     */
    public function edit(Fruit $fruit)
    {
        return view('fruits.edit',[
            'fruit' => $fruit
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fruit  $fruit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fruit $fruit)
    {
        $fruit = Fruit::find($fruit->id);
        $fruit->fruit = $request->input('fruit');
        $fruit->price = $request->input('price');
        $fruit->description = $request->input('description');
        
        if ($fruit->update()) {
            return redirect()->route('fruits.index')->with(['success' => 'Fruta '.$fruit->fruit.' actualizada exitosamente']);
        }
        /* echo $fruit.'<br>';
        echo "<pre>";
        var_dump($request->all());
        echo "</pre>"; */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fruit  $fruit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fruit $fruit)
    {
        $fruit = Fruit::find($fruit->id);
        if ($fruit->delete()) {
            return redirect()->route('fruits.index')->with(['success' => 'Fruta '.$fruit->fruit.' eliminada exitosamente']);
        }
    }
}
