<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id', 'desc')->get();
        return view('products.index', [
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product();
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->description = $request->input('description');
        
        $path = $request->file('path');
        
        $image_name = time().$path->getClientOriginalName();
        \Storage::disk('products')->put($image_name, \File::get($path));
        
        $product->path = $image_name;

        $product->save();
        
        return redirect()->route('products.index')->with(['success' => 'Producto creado correctamente']);

       /*  echo "<pre>";
        var_dump($request->all());
        echo "</pre>"; */
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, $id)
    {
        $product = Product::find($id);
        return view('products.edit', [
            'product' => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->description = $request->input('description');

        $path = $request->file('path');
        
        if ($path) {
            $image_name = time().$path->getClientOriginalName();
            \Storage::disk('products')->put($image_name, \File::get($path));
            $product->path = $image_name;
        }

        if ($product->update()) {
            return redirect()->route('products.index')->with(['success' => 'Producto '.$product->name.' editado correctamente']);
        }

        var_dump($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function getImgProduct($filename){
        $file = \Storage::disk('products')->get($filename);
        return new Response($file, 200);
    }
}
