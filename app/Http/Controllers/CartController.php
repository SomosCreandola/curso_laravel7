<?php

namespace App\Http\Controllers;
use App\Product;

use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id', 'desc')->paginate(6);
        return view('sales.index', [
            'products' => $products
        ]);
    }

    public function addcart(Request $request){
        if (session()->has('cart')) {

            $carrito = is_array(session()->get('cart')) ? session()->get('cart') : [];

            $cont = 0;      
            if (in_array($request->input('id'), $carrito)) {
                $cont++;
            } 
            
            if ($cont > 0) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Este elemento ya esta en el carrito',
                    'carrito' => $carrito
                ]);
            }

            array_push($carrito, $request->input('id'));

            session()->put('cart', $carrito);
            return response()->json([
                'status' => 'success',
                'message' => 'Añadido exitosamente',
                'carrito' => $carrito
            ]);
        }

        $carrito = array($request->input('id'));
        session()->put('cart', $carrito);
        return response()->json([
            'status' => 'success',
            'message' => 'Añadido a la cesta por primera vez',
            'carrito' => $carrito
        ]);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getImgProduct($filename){
        $file = \Storage::disk('products')->get($filename);
        return new Response($file, 200);
    }
}
