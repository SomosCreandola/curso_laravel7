@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            <div class="card mb-2">
                <div class="card-header">List clients</div>

                <div class="card-body table-responsive">

                    @if (count($clients))
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>name</th>
                                    <th>lastname</th>
                                    <th>document</th>
                                    <th>email</th>
                                    <th>phone</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($clients as $client)
                                    <tr>
                                        <td>{{ $client->id }}</td>
                                        <td>{{ $client->name }}</td>
                                        <td>{{ $client->lastname }}</td>
                                        <td>{{ $client->document }}</td>
                                        <td>{{ $client->email }}</td>
                                        <td>{{ $client->phone }}</td>
                                        <td>
                                            <a href="{{ route('clients.edit', ['client' => $client]) }}" class="btn btn-sm btn-warning">Update</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>  
                        </table>
                    @else
                        <ul>
                            <li>No hay clientes registrados!!!</li>
                        </ul>
                    @endif
                    
                </div>
            </div>
             {{ $clients->links() }} 
        </div>
    </div>
</div>
@endsection