@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            <div class="card mb-2">
                <div class="card-header">Lista de productos</div>

                <div class="card-body">

                    @if (count($products))
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Producto</th>
                                    <th>Precio</th>
                                    <th>Descripción</th>
                                    <th>imagen</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->description }}</td>
                                        <td>
                                            <img src="{{ route('products.path', [
                                                'filename' => $product->path
                                            ]) }}" class="img-responsive" alt="img" style="width:50px;">
                                        </td>
                                        <td>
                                            <a href="{{ url('products-edit/'.$product->id.'') }}" class="btn btn-sm btn-warning">Editar</a>
                                            {{-- <a href="{{ route('products.edit', ['id' => $product->id]) }}" class="btn btn-sm btn-warning">Editar</a> --}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>  
                        </table>
                    @else
                        <ul>
                            <li>No hay productos registrados!!!</li>
                        </ul>
                    @endif
                    
                </div>
            </div>
            {{-- {{ $fruits->links() }} --}}
        </div>
    </div>
</div>
@endsection