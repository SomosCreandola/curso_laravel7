<div class="products">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="section_title text-center">Popular on Little Closet</div>
            </div>
        </div>
        <div class="row page_nav_row">
            <div class="col">
                <div class="page_nav">
                    <ul class="d-flex flex-row align-items-start justify-content-center">
                        <li class="active"><a href="category.html">Women</a></li>
                        <li><a href="category.html">Men</a></li>
                        <li><a href="category.html">Kids</a></li>
                        <li><a href="category.html">Home Deco</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row products_row">
            
            <!-- Product -->
            @foreach ($products as $product)
            <div class="col-xl-4 col-md-6">
                <div class="product">
                    <div class="product_image"><img src="{{ route('products.path', [
                        'filename' => $product->path
                    ]) }}" style="height: 330px; width:100%;" alt=""></div>
                    <div class="product_content">
                        <div class="product_info d-flex flex-row align-items-start justify-content-start">
                            <div>
                                <div>
                                    <div class="product_name"><a href="product.html">{{ $product->name }}</a></div>
                                    <div class="product_category">In <a href="category.html">Category</a></div>
                                </div>
                            </div>
                            <div class="ml-auto text-right">
                                <div class="rating_r rating_r_4 home_item_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                <div class="product_price text-right">$ {{ number_format($product->price, 0) }}</div>
                            </div>
                        </div>
                        <div class="product_buttons">
                            <div class="text-right d-flex flex-row align-items-start justify-content-start">
                                <div class="product_button product_fav text-center d-flex flex-column align-items-center justify-content-center">
                                    <div><div><img src="images/heart_2.svg" class="svg" alt=""><div>+</div></div></div>
                                </div>
                                <div class="product_button product_cart text-center d-flex flex-column align-items-center justify-content-center add-cart" data-id="{{ $product->id }}">
                                    <div><div><img src="images/cart.svg" class="svg" alt=""><div>+</div></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- Product -->
            

        </div>
        <div class="row load_more_row">
            <div class="col">
                <div class="button load_more ml-auto mr-auto"><a href="#">load more</a></div>
            </div>
        </div>
    </div>
</div>