@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-2">
                <div class="card-header">{{ __('Lista de pruebas') }}</div>

                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th scope="col">Test</th>
                            <th scope="col">User</th>
                            <th scope="col">Description</th>
                            <th scope="col">Date</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($records as $test)
                              <tr>
                                <th>{{ $test->test }}</th>
                                <td>{{ $test->user }}</td>
                                <td>{{ $test->description }}</td>
                                <td>{{ $test->created_at }}</td>
                              </tr>
                            @endforeach
                        </tbody>
                      </table>
                    
                </div>
            </div>
            {{ $records->links() }}
        </div>
    </div>
</div>
@endsection
