@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif

            <div class="card mb-2">
                <div class="card-header">Lista de frutas</div>

                <div class="card-body">

                    @if (count($fruits))
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Fruta</th>
                                    <th>Precio</th>
                                    <th>Descripción</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($fruits as $fruit)
                                    <tr>
                                        <td>{{ $fruit->id }}</td>
                                        <td>{{ $fruit->fruit }}</td>
                                        <td>{{ $fruit->price }}</td>
                                        <td>{{ $fruit->description }}</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <a href="{{ route('fruits.edit', ['fruit' => $fruit]) }}" class="btn btn-warning">Editar</a>
                                                <form action="{{ route('fruits.destroy', ['fruit' => $fruit]) }}" method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <input type="submit" class="btn btn-danger" value="Eliminar">
                                                </form>
                                                {{-- <a href="{{ route('fruits.destroy', ['fruit' => $fruit]) }}" class="btn btn-danger">Eliminar</a> --}}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>  
                        </table>
                    @else
                        <ul>
                            <li>No hay frutas registradas!!!</li>
                        </ul>
                    @endif
                    
                </div>
            </div>
            {{ $fruits->links() }}
        </div>
    </div>
</div>
@endsection